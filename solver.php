<?php

use SudokuSolver\Entities\PuzzleBuilder;
use SudokuSolver\Exceptions\SudokuException;
use SudokuSolver\Workers\Logging\StdOutLogger;

require_once(__DIR__ . "/vendor/autoload.php");

const _ = "_";
/*
$pData = array(
    array( _, _, _,     _, _, _,    _, _, _ ), // Row 1
    array( _, _, _,     _, _, _,    _, _, _ ), // Row 2
    array( _, _, _,     _, _, _,    _, _, _ ), // Row 3

    array( _, _, _,     _, _, _,    _, _, _ ), // Row 4
    array( _, _, _,     _, _, _,    _, _, _ ), // Row 5
    array( _, _, _,     _, _, _,    _, _, _ ), // Row 6

    array( _, _, _,     _, _, _,    _, _, _ ), // Row 7
    array( _, _, _,     _, _, _,    _, _, _ ), // Row 8
    array( _, _, _,     _, _, _,    _, _, _ ), // Row 9
);
*/

// puzzle # 5 (Very Easy)
$puzzle5 = array(
    array( _, 7, _,     _, _, _,    2, 9, 4 ), // Row 1
    array( 2, 8, _,     _, 7, 6,    _, 5, 1 ), // Row 2
    array( _, 4, _,     3, 2, 9,    _, _, _ ), // Row 3

    array( 7, _, 3,     1, _, 8,    5, _, _ ), // Row 4
    array( 1, _, 2,     _, 5, _,    8, _, 3 ), // Row 5
    array( _, _, 8,     9, _, 2,    6, _, 7 ), // Row 6

    array( _, _, _,     2, 1, 4,    _, 7, _ ), // Row 7
    array( 6, 2, _,     7, 9, _,    _, 8, 5 ), // Row 8
    array( 9, 1, 7,     _, _, _,    _, 3, _ ), // Row 9
);

// puzzle # 92 (Hard)
$puzzle92 = array(
    array( _, _, 6,     _, _, 3,    _, _, _ ), // Row 1
    array( _, _, _,     5, _, _,    4, 6, 2 ), // Row 2
    array( 9, _, 5,     _, _, _,    _, _, _ ), // Row 3

    array( _, 7, _,     _, 5, _,    _, 8, _ ), // Row 4
    array( _, 6, _,     _, _, _,    _, 3, _ ), // Row 5
    array( _, 2, _,     _, 1, _,    _, 5, _ ), // Row 6

    array( _, _, _,     _, _, _,    1, _, 7 ), // Row 7
    array( 3, 1, 4,     _, _, 9,    _, _, _ ), // Row 8
    array( _, _, _,     8, _, _,    6, _, _ ), // Row 9
);

// puzzle # 130 (Hard)
$puzzle130 = array(
    array( _, _, 6,     8, _, _,    2, _, _ ), // Row 1
    array( _, 5, _,     4, _, _,    _, _, _ ), // Row 2
    array( _, 7, _,     9, _, _,    6, _, _ ), // Row 3

    array( 1, _, _,     _, 7, _,    _, _, 9 ), // Row 4
    array( 9, _, _,     _, _, _,    _, _, 8 ), // Row 5
    array( 3, _, _,     _, 6, _,    _, _, 5 ), // Row 6

    array( _, _, 8,     _, _, 4,    _, 7, _ ), // Row 7
    array( _, _, _,     _, _, 2,    _, 1, _ ), // Row 8
    array( _, _, 4,     _, _, 3,    9, _, _ ), // Row 9
);

// puzzle # 136 (Very Hard)
$puzzle136 = array(
    array( _, 5, _,     _, _, _,    _, _, 6 ), // Row 1
    array( _, _, _,     _, _, 1,    _, 3, _ ), // Row 2
    array( _, _, 4,     9, _, _,    _, _, _ ), // Row 3

    array( 8, _, _,     _, 7, _,    _, _, 4 ), // Row 4
    array( 1, _, _,     _, 4, _,    _, _, 9 ), // Row 5
    array( 6, _, _,     _, 5, _,    _, _, 2 ), // Row 6

    array( _, _, _,     _, _, 6,    1, _, _ ), // Row 7
    array( _, 2, _,     3, _, _,    _, _, _ ), // Row 8
    array( 7, _, _,     _, _, _,    _, 8, _ ), // Row 9
);

try {
    $stdOutLogger = new StdOutLogger();
    $builder = new PuzzleBuilder();
    $builder->SetLogger($stdOutLogger);

    $puzzle = $builder->FromArray($puzzle130);

    $puzzle->PrintToScreen();
    $puzzle->Solve();

    $puzzle->PrintToScreen();
} catch(Exception $e) {
    echo PHP_EOL;
    echo "===" . PHP_EOL;
    echo "EXCEPTION" . PHP_EOL;
    echo "===". PHP_EOL;
    echo "File: " . $e->getFile(). PHP_EOL;
    echo "Line: ". $e->getLine() . PHP_EOL;
    echo "Message: " . $e->getMessage() . PHP_EOL;

    if ($e instanceof SudokuException)
    {
        $cell = $e->GetCell();
        echo "Cell number: " . $cell->GetNumber() . PHP_EOL;

        $rowValues = $cell->GetRow()->GetValues();
        echo "Row values: " . json_encode($rowValues) . PHP_EOL;
    }


    echo "\n\n\n";

}
