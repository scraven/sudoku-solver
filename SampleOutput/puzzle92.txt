
.   .   6      .   .   3      .   .   .      
.   .   .      5   .   .      4   6   2      
9   .   5      .   .   .      .   .   .      

.   7   .      .   5   .      .   8   .      
.   6   .      .   .   .      .   3   .      
.   2   .      .   1   .      .   5   .      

.   .   .      .   .   .      1   .   7      
3   1   4      .   .   9      .   .   .      
.   .   .      8   .   .      6   .   .      

starting Solve()
solved cell 1: 2 using the BlockElimination rule
solved cell 14: 9 using the RowElimination rule
solved cell 25: 3 using the ColumnElimination rule
solved cell 37: 5 using the BlockElimination rule
solved cell 55: 6 using the BlockElimination rule
solved cell 71: 2 using the CellElimination rule
solved cell 73: 7 using the CellElimination rule
solved cell 78: 1 using the BlockElimination rule
solved cell 81: 3 using the BlockElimination rule
[Round 1] opCount = 53
solved cell 11: 3 using the ColumnElimination rule
solved cell 12: 7 using the BlockElimination rule
solved cell 15: 8 using the CellElimination rule
solved cell 41: 8 using the BlockElimination rule
solved cell 46: 8 using the ColumnElimination rule
solved cell 57: 8 using the ColumnElimination rule
solved cell 59: 3 using the ColumnElimination rule
solved cell 60: 5 using the BlockElimination rule
solved cell 74: 5 using the BlockElimination rule
solved cell 75: 2 using the BlockElimination rule
solved cell 77: 4 using the CellElimination rule
solved cell 80: 9 using the CellElimination rule
[Round 2] opCount = 47
solved cell 5: 7 using the CellElimination rule
solved cell 8: 1 using the CellElimination rule
solved cell 10: 1 using the CellElimination rule
solved cell 22: 1 using the BlockElimination rule
solved cell 23: 2 using the ColumnElimination rule
solved cell 24: 6 using the BlockElimination rule
solved cell 26: 7 using the CellElimination rule
solved cell 27: 8 using the CellElimination rule
solved cell 28: 4 using the CellElimination rule
solved cell 33: 2 using the CellElimination rule
solved cell 34: 9 using the CellElimination rule
solved cell 43: 2 using the BlockElimination rule
solved cell 52: 7 using the CellElimination rule
solved cell 56: 9 using the CellElimination rule
solved cell 58: 2 using the CellElimination rule
solved cell 62: 4 using the CellElimination rule
solved cell 67: 7 using the BlockElimination rule
solved cell 68: 6 using the CellElimination rule
solved cell 70: 8 using the BlockElimination rule
solved cell 72: 5 using the CellElimination rule
[Round 3] opCount = 66
solved cell 2: 8 using the BlockElimination rule
solved cell 4: 4 using the CellElimination rule
solved cell 7: 5 using the CellElimination rule
solved cell 9: 9 using the CellElimination rule
solved cell 20: 4 using the CellElimination rule
solved cell 40: 9 using the CellElimination rule
solved cell 42: 7 using the BlockElimination rule
solved cell 45: 4 using the RowElimination rule
solved cell 48: 9 using the BlockElimination rule
solved cell 49: 3 using the RowElimination rule
solved cell 51: 4 using the CellElimination rule
solved cell 54: 6 using the CellElimination rule
[Round 4] opCount = 24
solved cell 30: 3 using the BlockElimination rule
solved cell 31: 6 using the CellElimination rule
solved cell 36: 1 using the CellElimination rule
solved cell 39: 1 using the CellElimination rule
[Round 5] opCount = 5
[Round 6] opCount = 0
Solutions found: 57

2   8   6      4   7   3      5   1   9      
1   3   7      5   9   8      4   6   2      
9   4   5      1   2   6      3   7   8      

4   7   3      6   5   2      9   8   1      
5   6   1      9   8   7      2   3   4      
8   2   9      3   1   4      7   5   6      

6   9   8      2   3   5      1   4   7      
3   1   4      7   6   9      8   2   5      
7   5   2      8   4   1      6   9   3      

