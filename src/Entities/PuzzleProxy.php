<?php


namespace SudokuSolver\Entities;

use SudokuSolver\Exceptions\InvalidCellCoordinatesException;

/**
 * Class PuzzleProxy
 * @package SudokuSolver\Entities
 *
 * This is the class that defines the public API for the Puzzle object.
 * It restricts access to some of the internal workings of the puzzle, and only
 * exposes the methods necessary to solve and print the puzzle
 */
class PuzzleProxy extends Puzzle
{
    /** @var Puzzle */
    private $puzzle;

    /**
     * PuzzleProxy constructor.
     * @param Puzzle $puzzle
     */
    public function __construct(Puzzle $puzzle)
    {
        $this->puzzle = $puzzle;
    } // end function __construct

    /**
     * Solve the puzzle
     */
    public function Solve()
    {
        $this->puzzle->solve();
    } // end function Solve

    /**
     * @throws InvalidCellCoordinatesException
     *
     * Print the puzzle
     */
    public function PrintToScreen()
    {
        $this->puzzle->printToScreen();
    } // end function Print
} // end class PuzzleProxy