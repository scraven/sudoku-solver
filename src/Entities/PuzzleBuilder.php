<?php


namespace SudokuSolver\Entities;


use Exception;
use SudokuSolver\Exceptions\InvalidCellCoordinatesException;
use SudokuSolver\Exceptions\InvalidCellValueException;
use SudokuSolver\Exceptions\SudokuException;
use SudokuSolver\Workers\Logging\Logger;
use SudokuSolver\Workers\Logging\NullLogger;

/**
 * Class PuzzleBuilder
 * @package SudokuSolver\Entities
 */
class PuzzleBuilder extends Puzzle
{
    /**
     * PuzzleBuilder constructor.
     */
    public function __construct()
    {
        if (is_null($this->logger))
        {
            $this->logger = new NullLogger();
        }

        parent::__construct($this->logger);
    } // end function __construct

    /**
     * @param array $data
     * @return PuzzleProxy
     * @throws InvalidCellCoordinatesException
     * @throws InvalidCellValueException
     * @throws SudokuException
     */
    public function FromArray(array $data)
    {
        $puzzle = $this->newPuzzle();

        $rowsDone = 0;
        foreach ($data as $rowNum => $row)
        {
            if (!is_array($row))
            {
                continue;
            }

            foreach ($row as $colNum => $cellValue)
            {
                if ($cellValue == _ )
                {
                    continue;
                }

                $cell = $puzzle->GetCell($rowNum+1, $colNum+1);
                $cell->SetValue($cellValue);
            }

            $rowsDone++;
        }

        return new PuzzleProxy($puzzle);
    } // end function FromArray

    /**
     * @return Puzzle
     * @throws Exception
     */
    private function newPuzzle()
    {
        $puzzle = new Puzzle($this->logger);

        $columns = array();
        $cellNumber = 1;
        for ($rowNum = 1; $rowNum <= 9; $rowNum++)
        {
            $row = new Row("ROW" . $rowNum);
            for ($colNum = 1; $colNum <= 9; $colNum++)
            {
                if (!array_key_exists($colNum, $columns))
                {
                    $columns[$colNum] =  new Column("COL" . $colNum);
                }

                $cell = new Cell();
                $cell->SetNumber($cellNumber++);
                $cell->SetRow($row);
                $cell->SetColumn($columns[$colNum]);
                $cell->SetPuzzle($puzzle);

                $puzzle->cells[$rowNum][$colNum] = $cell;
                $puzzle->cellsList[] = $cell;
            }
        }

        $block = new Block("BLK1");
        $puzzle->cells[1][1]->SetBlock($block);
        $puzzle->cells[1][2]->SetBlock($block);
        $puzzle->cells[1][3]->SetBlock($block);
        $puzzle->cells[2][1]->SetBlock($block);
        $puzzle->cells[2][2]->SetBlock($block);
        $puzzle->cells[2][3]->SetBlock($block);
        $puzzle->cells[3][1]->SetBlock($block);
        $puzzle->cells[3][2]->SetBlock($block);
        $puzzle->cells[3][3]->SetBlock($block);

        $block = new Block("BLK2");
        $puzzle->cells[1][4]->SetBlock($block);
        $puzzle->cells[1][5]->SetBlock($block);
        $puzzle->cells[1][6]->SetBlock($block);
        $puzzle->cells[2][4]->SetBlock($block);
        $puzzle->cells[2][5]->SetBlock($block);
        $puzzle->cells[2][6]->SetBlock($block);
        $puzzle->cells[3][4]->SetBlock($block);
        $puzzle->cells[3][5]->SetBlock($block);
        $puzzle->cells[3][6]->SetBlock($block);

        $block = new Block("BLK3");
        $puzzle->cells[1][7]->SetBlock($block);
        $puzzle->cells[1][8]->SetBlock($block);
        $puzzle->cells[1][9]->SetBlock($block);
        $puzzle->cells[2][7]->SetBlock($block);
        $puzzle->cells[2][8]->SetBlock($block);
        $puzzle->cells[2][9]->SetBlock($block);
        $puzzle->cells[3][7]->SetBlock($block);
        $puzzle->cells[3][8]->SetBlock($block);
        $puzzle->cells[3][9]->SetBlock($block);

        $block = new Block("BLK4");
        $puzzle->cells[4][1]->SetBlock($block);
        $puzzle->cells[4][2]->SetBlock($block);
        $puzzle->cells[4][3]->SetBlock($block);
        $puzzle->cells[5][1]->SetBlock($block);
        $puzzle->cells[5][2]->SetBlock($block);
        $puzzle->cells[5][3]->SetBlock($block);
        $puzzle->cells[6][1]->SetBlock($block);
        $puzzle->cells[6][2]->SetBlock($block);
        $puzzle->cells[6][3]->SetBlock($block);

        $block = new Block("BLK5");
        $puzzle->cells[4][4]->SetBlock($block);
        $puzzle->cells[4][5]->SetBlock($block);
        $puzzle->cells[4][6]->SetBlock($block);
        $puzzle->cells[5][4]->SetBlock($block);
        $puzzle->cells[5][5]->SetBlock($block);
        $puzzle->cells[5][6]->SetBlock($block);
        $puzzle->cells[6][4]->SetBlock($block);
        $puzzle->cells[6][5]->SetBlock($block);
        $puzzle->cells[6][6]->SetBlock($block);

        $block = new Block("BLK6");
        $puzzle->cells[4][7]->SetBlock($block);
        $puzzle->cells[4][8]->SetBlock($block);
        $puzzle->cells[4][9]->SetBlock($block);
        $puzzle->cells[5][7]->SetBlock($block);
        $puzzle->cells[5][8]->SetBlock($block);
        $puzzle->cells[5][9]->SetBlock($block);
        $puzzle->cells[6][7]->SetBlock($block);
        $puzzle->cells[6][8]->SetBlock($block);
        $puzzle->cells[6][9]->SetBlock($block);

        $block = new Block("BLK7");
        $puzzle->cells[7][1]->SetBlock($block);
        $puzzle->cells[7][2]->SetBlock($block);
        $puzzle->cells[7][3]->SetBlock($block);
        $puzzle->cells[8][1]->SetBlock($block);
        $puzzle->cells[8][2]->SetBlock($block);
        $puzzle->cells[8][3]->SetBlock($block);
        $puzzle->cells[9][1]->SetBlock($block);
        $puzzle->cells[9][2]->SetBlock($block);
        $puzzle->cells[9][3]->SetBlock($block);

        $block = new Block("BLK8");
        $puzzle->cells[7][4]->SetBlock($block);
        $puzzle->cells[7][5]->SetBlock($block);
        $puzzle->cells[7][6]->SetBlock($block);
        $puzzle->cells[8][4]->SetBlock($block);
        $puzzle->cells[8][5]->SetBlock($block);
        $puzzle->cells[8][6]->SetBlock($block);
        $puzzle->cells[9][4]->SetBlock($block);
        $puzzle->cells[9][5]->SetBlock($block);
        $puzzle->cells[9][6]->SetBlock($block);

        $block = new Block("BLK9");
        $puzzle->cells[7][7]->SetBlock($block);
        $puzzle->cells[7][8]->SetBlock($block);
        $puzzle->cells[7][9]->SetBlock($block);
        $puzzle->cells[8][7]->SetBlock($block);
        $puzzle->cells[8][8]->SetBlock($block);
        $puzzle->cells[8][9]->SetBlock($block);
        $puzzle->cells[9][7]->SetBlock($block);
        $puzzle->cells[9][8]->SetBlock($block);
        $puzzle->cells[9][9]->SetBlock($block);

        return $puzzle;
    } // end function newPuzzle

    /**
     * @param Logger $logger
     *
     * Sets the logger for the puzzle being built
     */
    public function SetLogger(Logger $logger)
    {
        $this->logger = $logger;
    } // end function SetLogger
} // end class PuzzleBuilder