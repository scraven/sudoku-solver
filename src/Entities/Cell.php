<?php


namespace SudokuSolver\Entities;


use Exception;
use SudokuSolver\Exceptions\InvalidCellValueException;
use SudokuSolver\Exceptions\SudokuException;

/**
 * Class Cell
 * @package SudokuSolver\Entities
 */
class Cell
{
    /** @var int */
    private $number;

    /** @var int */
    private $value;

    /** @var array  */
    private $possibleValues = array(
        1   => true,
        2   => true,
        3   => true,
        4   => true,
        5   => true,
        6   => true,
        7   => true,
        8   => true,
        9   => true
    );

    /** @var Row */
    private $row;

    /** @var Column */
    private $col;

    /** @var Block */
    private $block;

    /** @var Puzzle */
    private $puzzle;

    /**
     * @param $n int
     */
    public function SetNumber($n)
    {
        $this->number = $n;
    } // end function SetNumber

    /**
     * @return int
     */
    public function GetNumber()
    {
        return $this->number;
    } // end function GetNumber

    /**
     * @param Puzzle $puzzle
     */
    public function SetPuzzle(Puzzle $puzzle)
    {
        $this->puzzle = $puzzle;
    } // end function SetPuzzle

    /**
     * @param $v int
     * @throws InvalidCellValueException
     * @throws SudokuException
     */
    public function SetValue($v)
    {
        if ($v < 1 || $v > 9)
        {
            $e = new InvalidCellValueException($v);
            $e->SetCell($this);

            throw $e;
        }

        if ($this->possibleValues[$v] !== true)
        {
            $e = new SudokuException("Invalid cell solution ($v): this value has been removed from possible values");
            $e->SetCell($this);

            throw $e;
        }

        if ($this->row->HasSolvedValue($v))
        {
            $e = new SudokuException("Duplicate value in row: $v");
            $e->SetCell($this);

            throw $e;

        }

        if ($this->col->HasSolvedValue($v))
        {
            $e = new SudokuException("Duplicate value in column: $v");
            $e->SetCell($this);

            throw $e;
        }

        if ($this->block->HasSolvedValue($v))
        {
            $e = new SudokuException("Duplicate value in block: $v");
            $e->SetCell($this);

            throw $e;
        }

        // Remove all other values as possibilities
        for ($i = 1; $i <=9; $i++)
        {
            if ($i == $v)
            {
                continue;
            }

            $this->RemovePossibleValue($i);
        }


        $this->value = $v;
        $this->puzzle->IncOpCount();

        $this->row->RemovePossibleValues($v);
        $this->col->RemovePossibleValues($v);
        $this->block->RemovePossibleValues($v);

    } // end function SetValue

    /**
     * @return int
     */
    public function GetValue()
    {
        return $this->value;
    } // end function GetValue

    /**
     * @param $v
     * @throws InvalidCellValueException
     */
    public function RemovePossibleValue($v)
    {
        if ($this->IsSolved())
        {
            return;
        }

        if ($v < 1 || $v > 9)
        {
            throw new InvalidCellValueException($v);
        }

        if ($this->possibleValues[$v] === false)
        {
            return;
        }

        $this->possibleValues[$v] = false;
        $this->puzzle->IncOpCount();

    } // end function RemovePossibleValue

    /**
     * @param Row $row
     * @throws Exception
     */
    public function SetRow(Row $row)
    {
        $row->AddCell($this);
        $this->row = $row;
    } // end function SetRow

    /**
     * @param Column $column
     * @throws Exception
     */
    public function SetColumn(Column $column)
    {
        $column->AddCell($this);
        $this->col = $column;
    } // end function SetColumn

    /**
     * @param Block $block
     * @throws Exception
     */
    public function SetBlock(Block $block)
    {
        $block->AddCell($this);
        $this->block = $block;
    } // end function SetBlock

    /**
     * @return Row
     */
    public function GetRow()
    {
        return $this->row;
    } // end function GetRow

    /**
     * @return Column
     */
    public function GetColumn()
    {
        return $this->col;
    } // end function GetColumn

    /**
     * @return Block
     */
    public function GetBlock()
    {
        return $this->block;
    } // end function GetBlock

    /**
     * @return array
     */
    public function GetPossibleValues()
    {
        return $this->possibleValues;
    } // end function GetPossibleValues

    /**
     * @return bool
     */
    public function IsSolved()
    {
        return ! is_null($this->value);
    } // end function IsSolved

    /**
     * @param $value
     * @return bool|mixed
     */
    public function CouldBe($value)
    {
        if (array_key_exists($value, $this->possibleValues))
        {
            return $this->possibleValues[$value];
        }

        return false;
    } // end function CouldBe

} // end class Cell