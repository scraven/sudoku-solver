<?php


namespace SudokuSolver\Entities;


use Exception;
use SudokuSolver\Exceptions\InvalidCellValueException;

/**
 * Class CellCollection
 * @package SudokuSolver\Entities
 */
class CellCollection
{
    /** @var string */
    private $name;

    /** @var Cell[] */
    private $cells = array();

    /**
     * CellCollection constructor.
     * @param $name string
     */
    public function __construct($name)
    {
        $this->name = $name;
    } // end function __construct

    /**
     * @param Cell $cell
     * @throws Exception
     */
    public function AddCell(Cell $cell)
    {
        if (count($this->cells) == 9)
        {
            throw new Exception("Cannot add new cell: {$this->name} is full");
        }

        $this->cells[] = $cell;
    } // end function AddCell

    /**
     * @param $v
     * @return bool
     */
    public function HasSolvedValue($v)
    {
        foreach ($this->cells as $cell)
        {
            if ($cell->GetValue() == $v)
            {
                return true;
            }
        }

        return false;
    } // end function HasSolvedValue

    /**
     * @return array
     */
    public function GetValues()
    {
        $values = array();
        foreach ($this->cells as $cell)
        {
            $v = $cell->GetValue();
            if (is_null($v))
            {
                $v = ".";
            }

            $values[] = $v;

        }

        return $values;
    } // end function GetValues

    /**
     * @param $v
     * @throws InvalidCellValueException
     */
    public function RemovePossibleValues($v)
    {
        foreach ($this->cells as $cell)
        {
            $cell->RemovePossibleValue($v);
        }
    } // end function RemovePossibleValues

    /**
     * @return Cell[]
     */
    public function GetCells()
    {
        return $this->cells;
    } // end function GetCells

    /**
     * @return string
     */
    public function GetName()
    {
        return $this->name;
    } // end function GetName
} // end class CellCollection