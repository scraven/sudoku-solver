<?php


namespace SudokuSolver\Entities;

use SudokuSolver\Exceptions\InvalidCellCoordinatesException;
use SudokuSolver\Exceptions\InvalidCellValueException;
use SudokuSolver\Exceptions\SudokuException;
use SudokuSolver\Workers\Logging\Logger;

/**
 * Class Puzzle
 * @package SudokuSolver\Entities
 */
class Puzzle
{
    /** @var Cell[][] */
    protected $cells = array();

    /** @var Cell[] */
    protected $cellsList = array();

    /** @var int */
    protected $opCount = 0;

    /** @var Logger */
    protected $logger;

    /**
     * Puzzle constructor.
     * @param Logger $logger
     */
    protected function __construct(Logger $logger)
    {
        $this->logger = $logger;
    } // end function __construct

    /**
     * @param $rowNum int
     * @param $colNum int
     * @return Cell
     * @throws InvalidCellCoordinatesException
     */
    protected function GetCell($rowNum, $colNum)
    {
        if ($rowNum < 1 || $rowNum > 9 || $colNum < 1 || $colNum > 9)
        {
            throw new InvalidCellCoordinatesException($rowNum, $colNum);
        }


        return $this->cells[$rowNum][$colNum];
    } // end function GetCell

    /**
     *  Increase the operations count for a round.
     *  An operation is either the solving of a cell, or the removing of a possible value from a cell.
     */
    public function IncOpCount()
    {
        $this->opCount++;
    } // end function IncOpCount

    /**
     *  Start a loop to solve the puzzle.
     *  The loop will stop if it goes an entire round with 0 operations.
     */
    protected function solve()
    {
        $this->logger->Debug("starting Solve()");
        $round = 0;
        $solutions = 0;
        $this->opCount = -1;
        while ($this->opCount != 0)
        {
            $round++;
            $this->opCount = 0;
            foreach ($this->cellsList as $cell)
            {
                $value = $this->solveCell($cell);
                if ($value)
                {
                    $solutions++;
                }

            }

            $this->logger->Info("[Round $round] opCount = " . $this->opCount);
        }

        $this->logger->Info("Solutions found: $solutions");

    } // end function solve

    /**
     * @param Cell $cell
     * @return int|null
     * @throws InvalidCellValueException
     * @throws SudokuException
     */
    protected function solveCell(Cell $cell)
    {
        if ($cell->IsSolved())
        {
            return null;
        }

        $value = $rule = null;

        if (is_null($value))
        {
            $rule = "CellElimination";
            $value = $this->solveCellByCellElimination($cell);
        }

        if (is_null($value))
        {
            $rule = "BlockElimination";
            $value = $this->solveCellByBlockElimination($cell);
        }

        if (is_null($value))
        {
            $rule = "RowElimination";
            $value = $this->solveCellByRowElimination($cell);
        }

        if (is_null($value))
        {
            $rule = "ColumnElimination";
            $value = $this->solveCellByColumnElimination($cell);
        }

        if ($value)
        {
            $cell->SetValue($value);
            $this->logger->Info("solved cell {$cell->GetNumber()}: $value using the $rule rule");
        }

        return $value;
    } // end function solveCell

    /**
     * @param Cell $cell
     * @return int|null
     */
    protected function solveCellByCellElimination(Cell $cell)
    {
        $value = null;
        $possibleCount = 0;
        foreach ($cell->GetPossibleValues() as $v => $possible)
        {
            if ($possible)
            {
                $value = $v;
                $possibleCount++;
            }
        }

        if ($possibleCount == 1)
        {
            return $value;
        }

        return null;
    } // end function solveCellByCellElimination

    /**
     * @param Cell $submittedCell
     * @return int|null
     */
    protected function solveCellByBlockElimination(Cell $submittedCell)
    {
        $cellPossibleValues = $submittedCell->GetPossibleValues();
        $block = $submittedCell->GetBlock();
        $confirmedValue = null;

        foreach ($cellPossibleValues as $possibleValue => $possible)
        {
            if (!$possible)
            {
                continue;
            }

            $possibleCount = 0;
            foreach ($block->GetCells() as $cell)
            {
                // skip the submitted cell, we already know that the value for the submitted cell
                // could be our possibleValue.  this will also reduce false positives in the event
                // that a different cell in this block could be solved using a possible value from
                // the submittedCell's pv list
                if ($cell->GetNumber() == $submittedCell->GetNumber())
                {
                    continue;
                }

                if ($cell->CouldBe($possibleValue))
                {
                    $possibleCount++;
                }
            }

            if ($possibleCount == 0)
            {
                $confirmedValue = $possibleValue;
                break;
            }
        }

        return $confirmedValue;
    } // end function solveCellByBlockElimination

    /**
     * @param Cell $submittedCell
     * @return int|null
     */
    protected function solveCellByRowElimination(Cell $submittedCell)
    {
        $cellPossibleValues = $submittedCell->GetPossibleValues();
        $row = $submittedCell->GetRow();
        $confirmedValue = null;

        foreach ($cellPossibleValues as $possibleValue => $possible)
        {
            if (!$possible)
            {
                continue;
            }

            $possibleCount = 0;
            foreach ($row->GetCells() as $cell)
            {
                // skip the submitted cell, we already know that the value for the submitted cell
                // could be our possibleValue.  this will also reduce false positives in the event
                // that a different cell in this block could be solved using a possible value from
                // the submittedCell's pv list
                if ($cell->GetNumber() == $submittedCell->GetNumber())
                {
                    continue;
                }

                if ($cell->CouldBe($possibleValue))
                {
                    $possibleCount++;
                }
            }

            if ($possibleCount == 0)
            {
                $confirmedValue = $possibleValue;
                break;
            }
        }

        return $confirmedValue;
    } // end function solveCellByRowElimination

    /**
     * @param Cell $submittedCell
     * @return int|null
     */
    protected function solveCellByColumnElimination(Cell $submittedCell)
    {
        $cellPossibleValues = $submittedCell->GetPossibleValues();
        $col = $submittedCell->GetColumn();
        $confirmedValue = null;

        foreach ($cellPossibleValues as $possibleValue => $possible)
        {
            if (!$possible)
            {
                continue;
            }

            $possibleCount = 0;
            foreach ($col->GetCells() as $cell)
            {
                // skip the submitted cell, we already know that the value for the submitted cell
                // could be our possibleValue.  this will also reduce false positives in the event
                // that a different cell in this block could be solved using a possible value from
                // the submittedCell's pv list
                if ($cell->GetNumber() == $submittedCell->GetNumber())
                {
                    continue;
                }

                if ($cell->CouldBe($possibleValue))
                {
                    $possibleCount++;
                }
            }

            if ($possibleCount == 0)
            {
                $confirmedValue = $possibleValue;
                break;
            }
        }

        return $confirmedValue;
    } // end function solveCellByColumnElimination

    /**
     * @throws InvalidCellCoordinatesException
     *
     * Prints the puzzle to the screen
     */
    protected function printToScreen()
    {
        $spacer = "   ";
        for ($rowNum = 1; $rowNum <= 9; $rowNum++)
        {
            echo PHP_EOL;
            for ($colNum = 1; $colNum <= 9; $colNum++)
            {
                $value = $this->GetCell($rowNum, $colNum)->GetValue();
                if (is_null($value))
                {
                    $value = ".";
                }

                echo $value . $spacer;

                if ($colNum % 3 == 0)
                {
                    echo $spacer;
                }
            }

            if ($rowNum % 3 == 0 )
            {
                echo PHP_EOL;
            }
        }

        echo PHP_EOL;
    } // end function printToScreen
} // end class Puzzle