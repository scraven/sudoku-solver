<?php


namespace SudokuSolver\Exceptions;


use Exception;

/**
 * Class InvalidCellValueException
 * @package SudokuSolver\Exceptions
 */
class InvalidCellValueException extends SudokuException
{
    /**
     * InvalidCellValueException constructor.
     * @param $v
     */
    public function __construct($v)
    {
        parent::__construct("Invalid cell value: $v");
    } // end function __construct
} // end class InvalidCellValueException