<?php


namespace SudokuSolver\Exceptions;


use Exception;

/**
 * Class InvalidCellCoordinatesException
 * @package SudokuSolver\Exceptions
 */
class InvalidCellCoordinatesException extends SudokuException
{
    /**
     * InvalidCellCoordinatesException constructor.
     * @param $x
     * @param $y
     */
    public function __construct($x, $y)
    {

        $data = json_encode(array(
            "x" => $x,
            "y" => $y
        ));

        $message = "Invalid cell coordinates: $data both X and Y must be between 1 and 9.";
        parent::__construct($message);

    } // end function __construct
} // end class InvalidCellCoordinatesException