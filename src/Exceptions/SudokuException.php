<?php


namespace SudokuSolver\Exceptions;


use Exception;
use SudokuSolver\Entities\Cell;

/**
 * Class SudokuException
 * @package SudokuSolver\Exceptions
 */
class SudokuException extends Exception
{
    /** @var Cell */
    protected $cell;

    /**
     * @param Cell $cell
     */
    public function SetCell(Cell $cell)
    {
        $this->cell = $cell;
    } // end function SetCell

    /**
     * @return Cell
     */
    public function GetCell()
    {
        return $this->cell;
    } // end function GetCell
} // end class SudokuException