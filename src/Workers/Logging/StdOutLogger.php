<?php


namespace SudokuSolver\Workers\Logging;

use SudokuSolver\Workers\Logging\Logger;

/**
 * Class StdOutLogger
 * @package SudokuSolver\Workers\Logging
 */
class StdOutLogger extends Logger
{
    /**
     * @param $message
     */
    protected function log($message)
    {
        echo $message;
    } // end function log
} // end class StdOutLogger