<?php


namespace SudokuSolver\Workers\Logging;

/**
 * Class Logger
 * @package SudokuSolver\Workers\Logging
 */
abstract class Logger
{
    /** @var int  */
    public static $LEVEL_DEBUG = 0;

    /** @var int  */
    public static $LEVEL_INFO = 1;

    /** @var int  */
    public static $LEVEL_WARNING = 2;

    /** @var int */
    protected $level;

    /**
     * @param $level
     */
    public function SetLevel($level)
    {
        switch($level)
        {
            case static::$LEVEL_INFO:
            case static::$LEVEL_DEBUG:
            case static::$LEVEL_WARNING:
                $this->level = $level;
        }
    } // end function SetLevel

    /**
     * @param $message
     */
    abstract protected function log($message);

    /**
     * @param $level
     * @param $message
     *
     * Pass to ->log method if the message is logged at an appropriate level
     */
    protected function maybeLog($level, $message)
    {
        if ($level >= $this->level)
        {
            $this->log($message . PHP_EOL);
        }
    }

    /**
     * @param $message
     *
     * Wrapper to add the DEBUG level
     */
    public function Debug($message)
    {
        $this->maybeLog(static::$LEVEL_DEBUG, $message);
    } // end function Debug

    /**
     * @param $message
     *
     * Wrapper to the INFO level
     */
    public function Info($message)
    {
        $this->maybeLog(static::$LEVEL_INFO, $message);
    } // end function Info

    /**
     * @param $message
     *
     * Wrapper to add the WARNING level
     */
    public function Warning($message)
    {
        $this->maybeLog(static::$LEVEL_WARNING, $message);
    } // end function Warning
} // end class Logger