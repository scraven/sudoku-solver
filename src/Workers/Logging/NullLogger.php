<?php


namespace SudokuSolver\Workers\Logging;

/**
 * Class NullLogger
 * @package SudokuSolver\Workers\Logging
 */
class NullLogger extends Logger
{
    /**
     * @param $message
     */
    protected function log($message)
    {
        // do nothing

    } // end function log
} // end class NullLogger